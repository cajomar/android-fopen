# Android-fopen #
Highjack `fopen(3)` to open an android asset file as described [here](http://www.50ply.com/blog/2013/01/19/loading-compressed-android-assets-with-file-pointer/).  
Add this as a subdirectory near the top of your `CMakeLists.txt`:
```cmake
cmake_minimum_required(VERSION XXX)
project(MYPROJECT)

# Assuming that android-fopen is in CMAKE_CURRENT_SOURCE_DIR's parent directory
# This creates a library named `android-fopen`, shared or static depends on the
# current value of BUILD_SHARED_LIBS.
# From now on every library created with `add_library` will have `fopen`
# replaced with android-fopen's fopen funtion.
add_subdirectory(../android-fopen android-fopen)
# ...
add_library(my-lib SHARED source.c)
# ...
```
and it will automatically replace `fopen` with a function that opens a file from an android asset manager.  
You will need to set that asset manager with a call to `android_fopen_set_asset_manager()`. If you're using `android_native_app_glue.c` (comes with the [Android NDK](https://developer.android.com/ndk/)), you can call it like this:
```c
#include <android_native_app_glue.h>
void android_fopen_set_asset_manager(AAssetManager* manager);

void android_main(struct android_app* app) {
    // ...
    android_fopen_set_asset_manager(app->activity->assetManager);
    // ...
}
```
